@extends('share.master')
@section('noi_dung')
    <div class="row" id="app">
        <div class="col-md-3">
            <div class="card border-primary border-bottom border-3 border-0">
                <div class="card-header text-center">
                    <b>Thêm Mới Tác Giả</b>
                </div>
                <div class="card-body">
                    <div class="mb-3">
                        <label class="form-label">Mã tác Giả</label>
                        <input v-model="add_tac_gia.ma_tac_gia" type="text" class="form-control"
                            placeholder="Nhập mã sách">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Tên Tên Giả</label>
                        <input v-model="add_tac_gia.ten_tac_gia" type="text" class="form-control"
                            placeholder="Nhập tên sách">
                    </div>
                    <div class="form-goup mb-3 ">
                        <label class="form-label">Ảnh</label>
                        <input class="form-control " type="file" ref="file" v-on:change="updateFile()"
                            accept="image/*">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Thông Tin</label>
                        <textarea v-model="add_tac_gia.thong_tin" class="form-control" cols="30" rows="5"></textarea>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Trạng Thái</label>
                        <select v-model="add_tac_gia.trang_thai" class="form-control">
                            <option value="-1">Vui lòng chọn trạng thái</option>
                            <option value="1">Hoạt Động</option>
                            <option value="0">Dừng Hoạt Động</option>
                        </select>
                    </div>
                </div>
                <div class="card-footer text-end">
                    <button v-on:click="addTacGia()" class="btn btn-primary">Thêm Mới</button>
                </div>

            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header text-center">
                    <b> Danh Sách Tác Giả</b>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="listNhaCungCap">
                        <thead>

                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Mã Tác giả</th>
                                <th class="text-center">Tên tác Giả</th>
                                <th class="text-center">Thông tin</th>
                                <th class="text-center">Hình Ảnh</th>
                                <th class="text-center">Trạng Thái</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="(value, key) in list_tac_gia">
                                <tr>
                                    <th class="text-center align-middle">
                                        <input type="checkbox" v-model="value.check">
                                    </th>
                                    <td class="align-middle text-nowrap"> @{{ value.ma_tac_gia }} </td>
                                    <td class="align-middle "> @{{ value.ten_tac_gia }} </td>
                                    <td class="align-middle "> @{{ value.thong_tin }} </td>
                                    <td class="text-center align-middle text-nowrap">
                                        <img v-bind:src="'/hinh-anh-tac-gia/' + value.hinh_anh"
                                            style="width: 70px; height: 70px;" alt="">
                                    </td>
                                    <td class="text-center align-middle text-nowrap">
                                        <button v-on:click="doiTrangThai(value)" type="button"
                                            class=" btn btn-outline-danger" v-if="value.trang_thai == 1">Hoạt Động</button>
                                        <button v-on:click="doiTrangThai(value)" type="button"
                                            class=" btn btn-outline-warning" v-else>Dừng Hoạt Động</button>
                                    </td>
                                    <td class="text-center align-middle text-nowrap">
                                        {{-- v-on:click="edit_Nha_Cung_Cap = Object.assign({}, value)" --}}
                                        <button class=" btn btn-outline-info btn-sm m-1"
                                            data-bs-toggle="modal"data-bs-target="#updateModal">Cập nhật</button>
                                        {{-- v-on:click="del_Nha_Cung_Cap = value" --}}
                                        <button class=" btn btn-outline-dark btn-sm"
                                            data-bs-toggle="modal"data-bs-target="#deleteModal">Huỷ bỏ</button>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            new Vue({
                el: '#app',
                data: {
                    list_tac_gia: [],
                    add_tac_gia: {
                        'ma_tac_gia': '',
                        'ten_tac_gia': '',
                        'thong_tin': '',
                        'hinh_anh': '',
                        'trang_thai': -1,

                    },
                    file: '',

                },
                created() {
                    this.loadDataTacGia();
                },
                methods: {
                    //Hàm thêm mới + upFile
                    updateFile() {
                        this.file = this.$refs.file.files[0];

                    },
                    addTacGia() {
                        var formData = new FormData();
                        formData.append('hinh_anh', this.file);
                        formData.append('ma_tac_gia', this.add_tac_gia.ma_tac_gia);
                        formData.append('ten_tac_gia', this.add_tac_gia.ten_tac_gia);
                        formData.append('thong_tin', this.add_tac_gia.thong_tin);
                        formData.append('trang_thai', this.add_tac_gia.trang_thai);

                        axios
                            .post('/admin/tac-gia/create', formData, {
                                headerf: {
                                    'Content-Type': 'multipart/form-data'
                                }
                            })
                            .then((res) => {
                                if (res.data.status == 1) {
                                    toastr.success(res.data.message, "Success");
                                    this.loadDataTacGia();
                                    this.add_tac_gia = {
                                        'ma_tac_gia': '',
                                        'ten_tac_gia': '',
                                        'thong_tin': '',
                                        'hinh_anh': '',
                                        'trang_thai': -1,
                                    };
                                }
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0]);
                                })
                                $("#add").removeAttr("disabled");
                            });
                    },
                    // Hàm đẩy dữ liệu lên serve
                    loadDataTacGia() {
                        axios
                            .get('/admin/tac-gia/data')
                            .then((res) => {
                                this.list_tac_gia = res.data.list;
                            });
                    },
                    //hàm đổi trạng thái và tình trạng
                    doiTrangThai(payLoad) {
                        axios
                            .post('/admin/tac-gia/doi-trang-thai', payLoad)
                            .then((res) => {
                                if (res.data.status) {
                                    toastr.success(res.data.message, "Success");
                                    this.loadDataTacGia();
                                }
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0]);
                                });
                            });
                    },
                    //Hàm cập nhật
                    // accpectUpdate() {
                    //     axios
                    //         .post('/admin/quan-ly-sach/update', this.edit_sach)
                    //         .then((res) => {
                    //             if (res.data.status) {
                    //                 toastr.success(res.data.message, "Success");
                    //                 this.loadData();
                    //                 $('#updateModal').modal('hide');
                    //             }
                    //         })
                    //         .catch((res) => {
                    //             $.each(res.response.data.errors, function(k, v) {
                    //                 toastr.error(v[0]);
                    //             })
                    //             $("#add").removeAttr("disabled");
                    //         });
                    // },
                    // accpectDelete() {
                    //     axios
                    //         .post('/admin/quan-ly-sach/delete', this.del_sach)
                    //         .then((res) => {
                    //             if (res.data.status) {
                    //                 toastr.success(res.data.message, "Success");
                    //                 this.loadData();
                    //                 $('#updateModal').modal('hide');
                    //             }
                    //         })
                    //         .catch((res) => {
                    //             $.each(res.response.data.errors, function(k, v) {
                    //                 toastr.error(v[0]);
                    //             });
                    //         });
                    // }
                }
            });
        });
    </script>
@endsection
