@extends('share.master')
@section('noi_dung')
    <div class="card" id="app">
        <div class="card-body">
            <div class="d-lg-flex align-items-center mb-4 gap-3">
                <div class="position-relative">
                    <input type="text" class="form-control ps-5 radius-30" placeholder="Search Order"> <span
                        class="position-absolute top-50 product-show translate-middle-y"><i class="bx bx-search"></i></span>
                </div>
                <div class="ms-auto"><a href="javascript:;" class="btn btn-primary radius-30 mt-2 mt-lg-0"><i
                            class="bx bxs-plus-square"></i>Thanh Toán</a></div>
            </div>
            <div class="table-responsive">
                <table class="table mb-0">
                    <thead class="table-light">
                        <tr>
                            <th class="text-center align-middle">#</th>
                            <th class="text-center align-middle">Tên Sách</th>
                            <th class="text-center align-middle" style="width: 20%;">Số Lượng</th>
                            <th class="text-center align-middle">Đơn Giá</th>
                            <th class="text-center align-middle">Giảm Giá</th>
                            <th class="text-center align-middle">Thành TIền</th>
                            <th class="text-center align-middle">ghi Chú</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <template v-for="(value, key) in listCart">
                            <tr>
                                <td class="text-center align-middle">
                                    <input class="form-check-input me-3" type="checkbox" v-model="value.check">
                                </td>
                                <td class="text-center align-middle"> @{{ value.ten_sach }} </td>
                                <td class="text-center align-middle">
                                    <input v-on:change="update(value)" v-model="value.so_luong_mua" type="number"
                                        class="form-control text-center">
                                </td>
                                {{-- <td class="text-center align-middle"> @{{ value.so_luong_mua }} </td> --}}
                                <td class="text-center align-middle"> @{{ number_format(value.don_gia_mua) }} </td>
                                <td class="text-center align-middle">
                                    <input v-on:change="update(value)" v-model="value.giam_gia" type="number"
                                        value="0" class="form-control text-center">
                                </td>
                                <td v-if="value.so_luong_mua <= 0" class="text-center align-middle">0đ</td>
                                <td v-else class="text-center align-middle"> @{{ number_format(value.tong_tien) }}</td>
                                <td class="text-center align-middle">
                                    <input v-on:change="update(value)" v-model="value.ghi_chu_loai_thanh_toan"
                                        type="text" class="form-control text-center">
                                </td>
                                <td>
                                    <a class="text-center align-middle" href="javascript:;" class=""><i
                                            class="bx bxs-edit"></i></a>
                                    <a v-on:click="del_sach = value" data-bs-toggle="modal"data-bs-target="#deleteModal"
                                        href="javascript:;" class="ms-3"><i class="bx bxs-trash"></i></a>
                                </td>
                            </tr>
                        </template>

                    </tbody>

                    <thead class="table-light">
                        <tr>
                            <th class="text-center align-middle"> </th>
                            <th class="text-center align-middle"> </th>
                            <th class="text-center align-middle"> </th>
                            <th class="text-center align-middle"> </th>
                            <th class="text-center align-middle"> </th>
                            <th class="text-center align-middle"> </th>
                            <th class="text-center align-middle"> </th>
                            <th class="text-center align-middle"> </th>
                        </tr>
                    </thead>
                </table>
                <div class="row no-gutters mt-2 mb-2">
                    <div class="h6 mb-0">Tổng Tiền: @{{ formatPrice(totalRequest()) }} VND </div>
                </div>
                <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="modal-title fs-5" id="exampleModalLabel">Xóa Khu Vục</h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-primary" role="alert">
                                    Bạn có chắc chắn muốn xóa sách: <b
                                        class="text-danger text-uppercase">@{{ del_sach.ten_sach }}</b> này không?
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-danger" v-on:click="accpectDelete()"
                                    data-bs-dismiss="modal">Xác Nhận</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="">
            <div class="row">
                <div class="col-md-7">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Họ và Tên</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                <input type="text" class="form-control" value="" v-model="ship_fullname">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Số Điện Thoại</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                <input type="number" class="form-control" value="" v-model="ship_phone">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Địa Chỉ</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                <input type="text" class="form-control" value="" v-model="ship_address">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Thanh Toán</h6>
                            </div>
                            <select v-model="is_payment" class="col-sm form-control">
                                <option value="-1">Vui Lòng Chọn Loại Thanh Toán</option>
                                <option value="0">Thanh Toán online</option>
                                <option value="1">Thanh Toán Khi Nhận Hàng</option>
                            </select>
                        </div>
                        <div class="row ">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9 text-secondary">
                                <a href="#" v-on:click="createBill()"
                                    class="btn btn-primary btn-rounded px-lg-5">Đặt hàng</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            new Vue({
                el: '#app',
                data: {
                    listCart: [],
                    del_sach: '',
                    ship_phone: '',
                    ship_fullname: '',
                    ship_address: '',
                    is_payment : 0,
                },
                created() {
                    this.loadData();
                },
                methods: {
                    number_format(number) {
                        return new Intl.NumberFormat('vi-VI', {
                            style: 'currency',
                            currency: 'VND'
                        }).format(number);
                    },
                    loadData() {
                        axios
                            .get('/admin/gio-hang/data-gio-hang')
                            .then((res) => {
                                this.listCart = res.data.data;
                            });
                    },
                    date_format(now) {
                        return moment(now).format('HH:mm:ss');
                    },
                    accpectDelete() {
                        axios
                            .post('/admin/gio-hang/delete', this.del_sach)
                            .then((res) => {
                                if (res.data.status) {
                                    toastr.success(res.data.message, "Success");
                                    this.loadData();
                                    $('#updateModal').modal('hide');
                                }
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0]);
                                });
                            });
                    },
                    update(v) {
                        axios
                            .post('/admin/ban-hang/update', v)
                            .then((res) => {
                                if (res.data.status == 0) {
                                    toastr.warning(res.data.message, "Warning");
                                } else if (res.data.status == 2) {
                                    toastr.error(res.data.message);
                                }
                                this.loadData();
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0]);
                                });
                            });
                    },
                    donGia(x, y) {
                        if (x == 0) {
                            return y;
                        } else {
                            return x;
                        }
                    },
                    thanhTien(v) {
                        return this.donGia(v.don_gia_mua, v.giam_gia) * v.so_luong_mua;
                    },
                    totalRequest() {
                        var total = 0;
                        for (var i in this.listCart) {
                            if (this.listCart[i]['check'] == true) {
                                total = total + this.thanhTien(this.listCart[i]) - this.listCart[i]
                                    .giam_gia;
                            }
                        }
                        return total;
                    },
                    createBill() {
                        const selectedItems = this.listCart.filter(item => item.check);
                        payload = {
                            ship_phone: this.ship_phone,
                            ship_fullname: this.ship_fullname,
                            ship_address: this.ship_address,
                            list_cart: selectedItems,
                            is_payment: this.is_payment,
                        };
                        axios
                            .post('{{ route('createBill') }}', payload)
                            .then((res) => {
                                if (res.data.status == 1) {
                                    toastr.success(res.data.message, "Success");
                                } else if (res.data.status == 0) {
                                    toastr.error(res.data.message, "Error");
                                } else if (res.data.status == 2) {
                                    toastr.warning(res.data.message, "Warning");
                                }
                            })
                            .catch((res) => {
                                var listError = res.response.data.errors;
                                $.each(listError, function(key, value) {
                                    toastr.error(value[0]);
                                });
                            });

                    },
                    formatPrice(value) {
                        let val = (value / 1).toFixed(0).replace('.', ',')
                        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                    },
                },
            });
        });
    </script>
@endsection
