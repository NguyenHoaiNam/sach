<?php

namespace App\Providers;

use App\Models\TacGia;
use App\Models\TheLoai;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        view()->composer('share.menu',function($view) {
            $theLoai  =    TheLoai::all();
            $view->with('theLoai',$theLoai);
        });
        view()->composer('share.menu',function($view) {
            $tacGia  =    TacGia::all();
            $view->with('tacGia',$tacGia);
        });
    }
}
