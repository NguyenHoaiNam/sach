<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuanLySach extends Model
{
    protected $table = 'quan_ly_sachs';
    protected $fillable = [
        'ma_sach',
        'ten_sach',
        'id_the_loai',
        'gia_ban',
        'ma_tac_gia',
        'hinh_anh',
        'thong_tin',
        'tinh_trang',
        'trang_thai',
        'so_luong_sach',
    ];
}
