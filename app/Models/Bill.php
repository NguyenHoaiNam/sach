<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $table = 'bills';
    protected $fillable = [
        'bill_name',
        'bill_total',
        'ten_sach',
        'so_luong',
        'khach_hang_id',
        'customer_email',
        'customer_fullname',
        'customer_phone',
        'ship_fullname',
        'ship_phone',
        'ship_address',
        'is_payment',
        'is_type',
        'ngay_thanh_toan',
    ];
}
