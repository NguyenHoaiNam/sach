<?php

namespace App\Http\Controllers;

use App\Http\Requests\Cart\UpdateCartRequest;
use App\Http\Requests\HoaDon\UpdateChiTietBanHangRequest;
use App\Models\ChiTietHoaDon;
use App\Models\HoaDonBanHang;
use App\Models\QuanLySach;
use App\Models\TacGia;
use App\Models\TheLoai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class HoaDonBanHangController extends Controller
{
    public function index()
    {
        return view('rocker.page.ban_hang.index');
    }

    // public function indexChiTiet()
    // {
    //     $theLoai = TheLoai::all();
    //     $tacGia  = TacGia::all();
    //     $sanPham = QuanLySach::join('the_loais', 'quan_ly_sachs.id_the_loai', 'the_loais.id')
    //         ->select('quan_ly_sachs.*', 'the_loais.ten_the_loai')
    //         ->orderBy('quan_ly_sachs.id')
    //         ->get();

    //     return view('rocker.page.mua_hang.index', compact('theLoai', 'sanPham', 'tacGia'));
    // }


    public function indexGetChiTietSanPham(Request $request)
    {
        $theLoai  =   QuanLySach::join('the_loais', 'quan_ly_sachs.id_the_loai', 'the_loais.id')
            ->where('quan_ly_sachs.id', $request->id)
            ->first();

        $tacGia  =   QuanLySach::join('tac_gias', 'quan_ly_sachs.ma_tac_gia', 'tac_gias.id')
            ->where('quan_ly_sachs.id', $request->id)
            ->first();

        $sanPham    =    QuanLySach::where('id', $request->id)->first();

        return view('rocker.page.mua_hang.index', compact('sanPham', 'theLoai', 'tacGia'));
    }

    public function indexTheLoai($id)
    {
        $spTheoLoai     =    QuanLySach::where('id_the_loai', $id)->get();
        
        return view('rocker.page.sp_the_loai.index', compact('spTheoLoai'));
    }

    public function indexTacGia($id)
    {
        $spTheoTacGia     =    QuanLySach::where('ma_tac_gia', $id)->get();

        return view('rocker.page.sp_theo_tac_gia.index', compact('spTheoTacGia'));
    }

    //By giỏ hàng
    public function ByGioHang(UpdateCartRequest $request)
    {
        $custommer = Auth::guard('custommer')->user();
        $sach = QuanLySach::find($request->id);
        // $hoaDonBanHang = HoaDonBanHang::where('id', $request->id)
        //     ->where('id_khach_hang', $custommer->id)
        //     ->first();
        $hoaDonBanHang = HoaDonBanHang::find($request->id);
        if ($sach) {
            $so_luong_mua = $request->so_luong_mua;
            if ($request->so_luong_mua <= $sach->so_luong_sach) {
                HoaDonBanHang::create([
                    'ma_hoa_don_ban_hang'               => str::uuid(),
                    'id_khach_hang'                     =>  $custommer->id,
                    'id_quan_ly_sach'                   =>  $request->id,
                    'ten_sach'                          =>  $request->ten_sach,
                    'so_luong_mua'                      =>  $request->so_luong_mua,
                    'don_gia_mua'                       =>  $sach->gia_ban,
                    'tong_tien'                         =>  $sach->gia_ban * $request->so_luong_mua,
                ]);

                return response()->json([
                    'status'    => 1,
                    'message'   => 'Đã Thêm Vào Giỏ Hàng',
                ]);
            } else {
                return response()->json([
                    'status'    => 2,
                    'message'   => 'Số Lượng Sách Trong Kho chỉ có ' . $sach->so_luong_sach ,
                ]);
            }
        }

        return response()->json([
            'status'    => 0,
            'message'   => 'Số Lượng sách phải lớn hơn 0!',
        ]);
    }

    public function getDataGioHang(Request $request)
    {
        $custommer = Auth::guard('custommer')->user();

        $data = HoaDonBanHang::select('hoa_don_ban_hangs.*')
            ->where('id_khach_hang', $custommer->id)
            ->get();
        return response()->json([
            'status' => 1,
            'data'   => $data,
        ]);
    }


    public function update(UpdateChiTietBanHangRequest $request)
    {
        $hoaDonBanHang   = HoaDonBanHang::find($request->id);
        $sach            = QuanLySach::find($hoaDonBanHang->id_quan_ly_sach);
        // dd($sach);
        $soLuongSach     = $sach->so_luong_sach;
        $soLuongMua      = $request->so_luong_mua;

        if ($hoaDonBanHang && $soLuongMua <= $soLuongSach) {
            $hoaDonBanHang->so_luong_mua                    = $request->so_luong_mua;
            $hoaDonBanHang->ghi_chu_loai_thanh_toan         = $request->ghi_chu_loai_thanh_toan;
            $hoaDonBanHang->giam_gia                        = $request->giam_gia;
            // $hoaDonBanHang->tong_tien                       =  $request->so_luong_mua * $request->don_gia_mua;

            if ($hoaDonBanHang->so_luong_mua >= 1) {
                $hoaDonBanHang->tong_tien                   =  $request->so_luong_mua * $request->don_gia_mua;
            } else {
                $hoaDonBanHang->tong_tien                   =  $request->so_luong_mua;
            }
            $hoaDonBanHang->save();

            if ($hoaDonBanHang->giam_gia <= $hoaDonBanHang->tong_tien) {
                $hoaDonBanHang->tong_tien = $hoaDonBanHang->tong_tien - $request->giam_gia;
                $hoaDonBanHang->save();
            } else {
                $hoaDonBanHang->giam_gia = 0;
                return response()->json([
                    'status'    => 0,
                    'message'   => 'Tiền chiết khấu chỉ được tối đa: ' . number_format($hoaDonBanHang->tong_tien, 0, '.', '.') . 'đ',
                ]);
            }

            return response()->json([
                'status'    => 1,
            ]);
        } {
            return response()->json([
                'status'    => 2,
                'message'   => 'Số lượng sách trong kho chỉ có ' . $soLuongSach,
            ]);
        }
    }

    public function destroy(Request $request)
    {
        $hoaDonBanHang    =  HoaDonBanHang::find($request->id);

        if ($hoaDonBanHang) {
            $hoaDonBanHang->delete();
            return response()->json([
                'status'    => true,
                'message'   => "Đã xoá sách thành công khỏi giỏ hàng",
            ]);
        } else {
            return response()->json([
                'status'    => true,
                'message'   => "Không thể xoá",
            ]);
        }
    }
}
