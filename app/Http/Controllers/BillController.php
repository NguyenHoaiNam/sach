<?php

namespace App\Http\Controllers;

use App\Http\Requests\Bill\TotalRequest as BillTotalRequest;
use App\Http\Requests\TotalRequest;
use App\Models\Bill;
use App\Models\HoaDonBanHang;
use App\Models\QuanLySach;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpParser\Node\Stmt\TryCatch;
use Illuminate\Support\Str;

class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    // public function store(Request $request)
    // {
    //     // dd($request->all());
    //     DB::beginTransaction(); // Bắt đầu giao dịch trong cơ sở dữ liệu
    //     try {
    //         $list_cart = $request->list_cart;
    //         // $check = $request->check; //


    //         foreach ($list_cart as $key => $value) {
    //             if (!isset($value->check)) {
    //                 array_splice($list_cart, $key, 1);
    //             }
    //         }

    //         if (count($list_cart) > 0) {
    //             $khachHang = Auth::guard('custommer')->user();

    //             $bill = Bill::create([
    //                 'ship_fullname' => $request->ship_fullname,
    //                 'ship_address' => $request->ship_address,
    //                 'ship_phone' => $request->ship_phone,
    //             ]);

    //             $check = true;
    //             $tongTien = 0;

    //             $cartItems = HoaDonBanHang::where('id_khach_hang', $khachHang->id)
    //                 ->get();

    //             foreach ($cartItems as $key => $value) {
    //                 foreach ($list_cart as $value_cart) {
    //                     if ($value->id = $value_cart['id']) {
    //                         $sanPham = QuanLySach::find($value->id_quan_ly_sach);
    //                         // $hoaDon = HoaDonBanHang::find($value_cart->id_quan_ly_sach);

    //                         if ($sanPham && $sanPham->so_luong_sach >= $value->so_luong_mua) {
    //                             $khuyenMai = $sanPham->gia_ban;
    //                             $value->ten_sach = $sanPham->ten_sach;
    //                             $value->don_gia_mua = $khuyenMai;
    //                             $value->id_hoa_don_ban_hang = $bill->id;
    //                             $value->save();

    //                             $tongTien += $khuyenMai * $value->so_luong_mua;
    //                         } else {
    //                             $check = false;
    //                         }
    //                     }
    //                 }
    //             }

    //             //Cập nhật thông tin của đơn hàng và lưu vào sql.
    //             $bill->bill_name = "HDQL" . (158935 + $bill->id);
    //             $bill->bill_total = $tongTien;
    //             $bill->save();

    //             return response()->json([
    //                 'status' => true,
    //                 'message' => 'Đã tạo đơn hàng thành công!',
    //             ]);
    //         } else {

    //             return response()->json([
    //                 'status' => 0,
    //                 'message' => 'Số Lượng Trong Giỏ Hàng Không đủ!',
    //             ]);
    //         }
    //     } catch (Exception $e) {
    //         DB::rollBack();
    //         // Log::error($e->getMessage("Lỗi"));
    //         return response()->json([
    //             'status' => 2,
    //             'message' => '...!',
    //         ]);
    //     }
    // }

    public function store(Request $request)
    {
        $list_cart = $request->list_cart;
        // dd($list_cart);
        $hoaDon    = HoaDonBanHang::where('id', $list_cart)->first();
        // dd($hoaDon);
        $khachHang = Auth::guard('custommer')->user();
        if (count($list_cart) > 0) {

            $bill = Bill::create([
                'khach_hang_id'         => $hoaDon->id_khach_hang,
                'bill_name'             => 'NhaSachHoaiNam - ' . Str::uuid(),
                'ten_sach'              => $hoaDon->ten_sach,
                'so_luong'              => $hoaDon->so_luong_mua,
                'customer_email'        => $khachHang->email,
                'customer_fullname'     => $khachHang->ho_va_ten,
                'customer_phone'        => $khachHang->so_dien_thoai,
                'ship_phone'            => $request->ship_phone,
                'ship_fullname'         => $request->ship_fullname,
                'ship_address'          => $request->ship_address,
                'bill_total'            => $hoaDon->tong_tien,
                'is_payment'            => $request->is_payment,
                'ngay_thanh_toan'       => $hoaDon->ngay_thanh_toan = Carbon::now(),

            ]);
            $hoaDon->delete();
            // dd($bill);
            return response()->json([
                'status' => true,
                'message' => 'Đã tạo đơn hàng thành công!',
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => '.....!',
            ]);
        }
    }
}
