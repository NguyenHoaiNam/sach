<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Models\KhachHang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ThongKeController extends Controller
{
    public function index()
    {

        $data = KhachHang::join('bills', 'khach_hangs.id', 'bills.khach_hang_id')
            ->select('khach_hangs.ma_khach_hang', 'khach_hangs.ho_va_ten', DB::raw('SUM(bills.bill_total) as bill_total'))
            ->groupBy('khach_hangs.ma_khach_hang', 'khach_hangs.ho_va_ten')
            ->orderBy('bill_total', 'asc')
            ->take(10)
            ->get();

        $list_ten = [];
        $list_tien = [];

        foreach ($data as $key => $value) {
            array_push($list_ten, $value->ho_va_ten);
            array_push($list_tien, $value->bill_total);
        }
        return view('rocker.admin.thong_ke.thong_ke', compact('list_ten', 'list_tien'));
    }

    public function thongkeChart(Request $request)
    {
        $data = KhachHang::join('bills', 'khach_hangs.id', 'bills.khach_hang_id')
            ->whereDate('bills.ngay_thanh_toan', '>=', $request->begin)
            ->whereDate('bills.ngay_thanh_toan', '<=', $request->end)
            ->select('khach_hangs.ma_khach_hang', 'khach_hangs.ho_va_ten', DB::raw('SUM(bills.bill_total) as bill_total'))
            ->groupBy('khach_hangs.ma_khach_hang', 'khach_hangs.ho_va_ten')
            ->orderBy('bill_total', 'asc')
            ->take(10)
            ->get();
        // $data = KhachHang::join('bills', 'khach_hangs.id', 'bills.khach_hang_id')
        //     ->whereDate('bills.ngay_thanh_toan', '>=', $request->begin)
        //     ->whereDate('bills.ngay_thanh_toan', '<=', $request->end)
        //     ->select('khach_hangs.ma_khach_hang', 'khach_hangs.ho_va_ten', DB::raw('SUM(bills.bill_total) as bill_total'))
        //     ->groupBy('khach_hangs.ma_khach_hang', 'khach_hangs.ho_va_ten')
        //     ->orderBy('bill_total', 'asc')
        //     ->take(10)
        //     ->get();

        $list_ten = [];
        $list_tien = [];

        foreach ($data as $key => $value) {
            array_push($list_ten, $value->ho_va_ten);
            array_push($list_tien, $value->bill_total);
        }

        return view('rocker.admin.thong_ke.thong_ke', compact('list_ten', 'list_tien'));
    }

    public function indexSach()
    {
        return view('rocker.admin.thong_ke.thong_ke_sach');
    }

    public function thongKeSach(Request $request)
    {
        $data = Bill::whereDate('bills.ngay_thanh_toan', '>=', $request->begin)
            ->whereDate('bills.ngay_thanh_toan', '<=', $request->end)
            ->select(
                'bills.ten_sach',
                'bills.so_luong',
                'bills.bill_total'
            )
            ->groupBy('bills.ten_sach')
            ->get();

        return response()->json([
            'status'    => 1,
            'message'   => 'Đã lấy dữ liệu',
            'data'      => $data,
        ]);
    }
}
