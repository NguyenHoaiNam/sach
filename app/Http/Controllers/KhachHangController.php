<?php

namespace App\Http\Controllers;

use App\Http\Requests\KhachHang\CreateKhachHangRequest;
use App\Http\Requests\KhachHang\UpdateKhachHangRequest;
use App\Models\KhachHang;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use function GuzzleHttp\Promise\all;
use Illuminate\Support\Facades\Auth;


class KhachHangController extends Controller
{

    public function index()
    {
        return view('rocker.admin.khach_hang.index');
    }


    public function getData()
    {
        $data   =   KhachHang::get();

        return response()->json([
            'data' => $data,
        ]);
    }

    public function store(CreateKhachHangRequest $request)
    {
        $data = $request->all();
        // dd($data);
        if (isset($data['ho_lot'])) {
            $data['ho_va_ten'] = $data['ho_lot'] . " " . $data['ten_khach'];
        } else {
            $data['ho_va_ten'] = $data['ten_khach'];
        }
        $data['ma_khach_hang']  = Str::uuid();
        $data['password'] =  bcrypt($request->password);
        KhachHang::create($data);

        return response()->json([
            'status'    => true,
            'message'   => 'Đã tạo mới thành công!',
        ]);
    }

    public function update(UpdateKhachHangRequest $request)
    {
        $khachHang   =  KhachHang::where('id', $request->id)->first();

        if (isset($khachHang)) {
            $data = $request->all();
            if (isset($data['ho_lot'])) {
                $data['ho_va_ten'] = $data['ho_lot'] . " " . $data['ten_khach'];
            } else {
                $data['ho_va_ten'] = $data['ten_khach'];
            }
            $khachHang->update($data);

            return response()->json([
                'status'    => true,
                'message'   => 'Cập nhật khách hàng thành công!',
            ]);
        } else {
            return response()->json([
                'status'    => false,
                'message'   => 'Khách hàng không tồn tại!',
            ]);
        }
    }

    public function delete(Request $request)
    {
        $khachHang   = KhachHang::find($request->id);

        $khachHang->delete();

        return response()->json([
            'status'    => true,
            'message'   => 'Đã xoá khách hàng!',
        ]);
    }

    ///Login
    public function viewLogin()
    {
        $check = Auth::guard('custommer')->check();
        if ($check) {
            return redirect('/trang-chu');
        } else {
            return view('Login_custommer.login');
        }
    }

    public function actionLogin(Request $request)
    {
        $check  = Auth::guard('custommer')->attempt([
            'email'    => $request->email,
            'password'    => $request->password,
        ]);

        $email = $request->input('email');
        $password = $request->input('password');
        if (!empty($email)) {
            if (!empty($password)) {
                if ($check) {
                    toastr()->success("Đã đăng nhập thành công");
                    return redirect('/trang-chu');
                } else {
                    toastr()->error("Tên đăng nhập hoặc mật khẩu không đúng");
                    return redirect('/custommer-login');
                }
            } else {
                toastr()->error("Bạn chưa nhập password");
                return redirect('/custommer-login');
            }
        } else {
            toastr()->error("Bạn chưa nhập email");
            return redirect('/custommer-login');
        }
    }

    public function actionLogOut()
    {

        Auth::guard('custommer')->logout();

        toastr()->success("Đã đăng xuất tài khoản");
        return redirect('/custommer-login');
    }
}
