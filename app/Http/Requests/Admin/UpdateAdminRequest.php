<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAdminRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

            'id'                =>  'required|exists:admins,id',
            'email'             =>  'email|unique:admins,email,' . $this->id,
            'ho_va_ten'         =>  'required|min:5',
            'so_dien_thoai'     =>  'required|digits:10||regex:/^[0-9]+$/',
            'ngay_sinh'         =>  'required|date',
            // 'id_quyen'          =>  'required',
        ];
    }

    public function messages()
    {
        return [
            'id.*'                      => 'Nhà cung cấp không tồn tại!',
            'email.required'            =>  'Email không được để trống!',
            'email.email'               =>  'Email không đúng định dạng!',
            'ho_va_ten.*'               =>  'Họ và tên không được để trống!',
            'so_dien_thoai.required'    =>  'Số điện thoại không được để trống!',
            'so_dien_thoai.digits'      =>  'Số điện thoại phải là 10 số!',
            'so_dien_thoai.regex'       =>  'Số điện thoại phải là số!',
            'ngay_sinh.*'               =>  'Ngày sinh không được để trống!',
            // 'id_quyen.*'          =>  'Không được để trống quyền!',
        ];
    }
}
