<?php

namespace App\Http\Requests\Cart;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCartRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            // 'id'            =>  'required|exists:hoa_don_ban_hang,id',
            'so_luong_mua'  =>  'required|numeric|min:1',
        ];
    }
    public function messages()
    {
        return [
            'so_luong_mua.*'        => 'Số lượng mua không được nhỏ hơn 1',
        ];
    }
}
