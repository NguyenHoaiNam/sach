<?php

namespace App\Http\Requests\KhachHang;

use Illuminate\Foundation\Http\FormRequest;

class UpdateKhachHangRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id'                => 'required|exists:khach_hangs,id',
            'ho_lot'            => 'nullable',
            'ten_khach'         => 'required',
            'so_dien_thoai'     => 'required||digits:10|regex:/^[0-9]+$/',
            'email'             => 'required|email',
            'ghi_chu'           => 'nullable',
            'ngay_sinh'         => 'nullable|date',
            'id_loai_khach'     => 'required',
            'ma_so_thue'        => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'id.*'                      => 'Khách hàng không tồn tại!',
            'ten_khach.*'               => 'Tên Khách hông được để trống!',
            'so_dien_thoai.require'     => 'Số điện thoại Không được để trống!',
            'so_dien_thoai.digits'      => 'Số điện thoại phải là 10 số!',
            'so_dien_thoai.regex'       => 'Số điện thoại phải là số!',
            'email.required'            => 'Email bắt buộc phải có!',
            'email.email'               => 'Email không đúng định dạng!',
            'ngay_sinh.date'            => 'Ngày sinh không đúng định dạng!',
            'id_loai_khach.*'           => 'Loại khách không được để trống!',
        ];
    }
}
