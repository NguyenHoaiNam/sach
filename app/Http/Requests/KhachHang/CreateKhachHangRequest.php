<?php

namespace App\Http\Requests\KhachHang;

use Illuminate\Foundation\Http\FormRequest;

class CreateKhachHangRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'ho_lot'            => 'nullable',
            'ten_khach'         => 'required',
            'so_dien_thoai'     => 'required||digits:10|regex:/^[0-9]+$/',
            'email'             => 'required|email|unique:admins,email',
            'password'          =>  'required',
            're_password'       =>  'required|same:password',
            'ghi_chu'           => 'nullable',
            'ngay_sinh'         => 'nullable|date',
            // 'id_loai_khach'     => 'required',
            'ma_so_thue'        => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'ten_khach.*'               => 'Tên Khách hông được để trống!',
            // 'so_dien_thoai.*'        => 'Số điện thoại Không được để trống!',
            // 'id_loai_khach.*'           => 'Loại kháchkhông được để trống!',
            'email.required'            =>  'Email không được để trống!',
            'email.email'               =>  'Email không đúng định dạng!',
            'email.unique'              =>  'Email đã tồn tại trong hệ thống!',
            'so_dien_thoai.required'    =>  'Số điện thoại bắt buộc phải có!',
            'so_dien_thoai.digits'      =>  'Số điện thoại phải là 10 số!',
            'so_dien_thoai.regex'       =>  'Số điện thoại phải số!',
            'ngay_sinh.date'            =>  'Ngày sinh không đúng định dạng!',
            'password.*'                =>  'Mật khẩu không được để trống!',
            're_password.*'             =>  'Mật khẩu không trùng khớp!',
        ];
    }
}
