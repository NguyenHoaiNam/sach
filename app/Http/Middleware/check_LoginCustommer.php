<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class check_LoginCustommer
{

    public function handle(Request $request, Closure $next): Response
    {
        $check = Auth::guard('custommer')->check();
        if ($check) {
            return $next($request);
        } else {
            toastr()->error("Yêu cầu phải đăng nhập!");
            return redirect('/custommer-login');
        }
    }
}
